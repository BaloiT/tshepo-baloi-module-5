import 'package:app_system/dashboardScreen.dart';
import 'package:app_system/homeScreen.dart';
import 'package:app_system/welcomeScreen.dart';
import 'package:flutter/material.dart';

class Feature1 extends StatefulWidget {
  const Feature1({Key? key}) : super(key: key);

  @override
  State<Feature1> createState() => _Feature1State();
}

class _Feature1State extends State<Feature1> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Feature 1'),
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: Colors.white,
          ),
          onPressed: () {
            Navigator.pop(context,
                MaterialPageRoute(builder: (context) => const Dashboard()));
          },
        ),
        actions: <Widget>[
          IconButton(icon: const Icon(Icons.settings), onPressed: () {}),
        ],
      ),
    );
  }
}
