import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class InputList extends StatefulWidget {
  const InputList({Key? key}) : super(key: key);

  @override
  State<InputList> createState() => _InputListState();
}

class _InputListState extends State<InputList> {
  final Stream<QuerySnapshot> _myAddInput =
      FirebaseFirestore.instance.collection("Details").snapshots();

  @override
  Widget build(BuildContext context) {
    TextEditingController _subjectFieldCtrlr = TextEditingController();
    TextEditingController _locationFieldCtrlr = TextEditingController();

    void _delete(docId) {
      FirebaseFirestore.instance
          .collection("Details")
          .doc(docId)
          .delete()
          .then((value) => print("Record deleted"));
    }

    void _update(data) {
      var collection = FirebaseFirestore.instance.collection("Details");
      _subjectFieldCtrlr.text = data["Subject_Name"];
      _locationFieldCtrlr.text = data["location"];
      showDialog(
          context: context,
          builder: (_) => AlertDialog(
                title: Text("Update"),
                content: Column(mainAxisSize: MainAxisSize.min, children: [
                  TextField(
                    controller: _subjectFieldCtrlr,
                  ),
                  TextField(
                    controller: _locationFieldCtrlr,
                  ),
                  TextButton(
                      onPressed: () {
                        collection.doc(data["doc_id"]).update({
                          "Subject_Name": _subjectFieldCtrlr.text,
                          "location": _locationFieldCtrlr.text
                        });
                        Navigator.pop(context);
                      },
                      child: Text("Update"))
                ]),
              ));
    }

    return StreamBuilder(
      stream: _myAddInput,
      builder: (BuildContext context,
          AsyncSnapshot<QuerySnapshot<Object?>> snapshot) {
        if (snapshot.hasError) {
          return Text("Something went wrong");
        }
        if (snapshot.connectionState == ConnectionState.waiting) {
          return CircularProgressIndicator();
        }
        if (snapshot.hasData) {
          return Row(
            children: [
              Expanded(
                  child: SizedBox(
                      height: (MediaQuery.of(context).size.height),
                      width: MediaQuery.of(context).size.width,
                      child: ListView(
                        children: snapshot.data!.docs
                            .map((DocumentSnapshot documentSnapshot) {
                          Map<String, dynamic> data =
                              documentSnapshot.data()! as Map<String, dynamic>;
                          return Column(
                            children: [
                              Card(
                                child: Column(
                                  children: [
                                    ListTile(
                                      title: Text(data['Subject_Name']),
                                      subtitle: Text(data['location']),
                                    ),
                                    ButtonTheme(
                                      child: ButtonBar(
                                        children: [
                                          OutlineButton.icon(
                                            onPressed: () {
                                              _update(data);
                                            },
                                            icon: Icon(Icons.edit),
                                            label: Text("Edit"),
                                          ),
                                          OutlineButton.icon(
                                            onPressed: () {
                                              _delete(data["doc_id"]);
                                            },
                                            icon: Icon(Icons.remove),
                                            label: Text("Delete"),
                                          )
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              )
                            ],
                          );
                        }).toList(),
                      )))
            ],
          );
        } else {
          return (Text("No data"));
        }
      },
    );
  }
}
