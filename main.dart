// ignore: unused_import
import 'package:app_system/dashboardScreen.dart';
import 'package:app_system/homeScreen.dart';
import 'package:app_system/welcomeScreen.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
      options: FirebaseOptions(
          apiKey: "AIzaSyD89BwVawyYTNUJiP_UmG0ZFLOFgvMjROY",
          authDomain: "tshepobaloim5-20b7e.firebaseapp.com",
          projectId: "tshepobaloim5-20b7e",
          storageBucket: "tshepobaloim5-20b7e.appspot.com",
          messagingSenderId: "936862659157",
          appId: "1:936862659157:web:a3051f0f33d899d4669adc"));
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter App',
      theme: ThemeData(
        // Define the default brightness and colors.
        brightness: Brightness.light,
        primaryColor: Color.fromARGB(255, 17, 194, 164),

        // Define the default font family.
        fontFamily: 'Georgia',

        // Define the default `TextTheme`. Use this to specify the default
        // text styling for headlines, titles, bodies of text, and more.
        textTheme: const TextTheme(
          headline1: TextStyle(fontSize: 72.0, fontWeight: FontWeight.bold),
          headline6: TextStyle(fontSize: 28.0, fontStyle: FontStyle.italic),
          bodyText2: TextStyle(fontSize: 14.0, fontFamily: 'Hind'),
        ),
      ),
      home: SplashScreen(),
    );
  }
}

class SplashScreen extends StatelessWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AnimatedSplashScreen(
      splash: Column(
        children: [
          Image.asset('assets/images.jpg'),
          const Text(
            'Electricity app',
            style: TextStyle(fontSize: 40, fontWeight: FontWeight.bold),
          )
        ],
      ),
      backgroundColor: Color.fromARGB(144, 17, 212, 105),
      nextScreen: const Home(),
      splashIconSize: 250,
      duration: 3000,
      splashTransition: SplashTransition.rotationTransition,
    );
  }
}
