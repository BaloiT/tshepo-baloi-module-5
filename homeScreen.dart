import 'package:app_system/Addinput.dart';

import 'package:app_system/dashboardScreen.dart';
import 'package:app_system/feature1.dart';
import 'package:app_system/feature2.dart';
import 'package:app_system/loginScreen.dart';
import 'package:app_system/main.dart';
import 'package:app_system/signupScreen.dart';
import 'package:app_system/userProfileScreen.dart';
import 'package:app_system/welcomeScreen.dart';
//import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int currentTab = 0;
  final List<Widget> screens = [
    LoginScreen(),
    SignUpScreen(),
    Dashboard(),
    Feature1(),
    Feature2(),
    EditProfile(),
    AddInput()
  ];

  final PageStorageBucket bucket = PageStorageBucket();
  Widget currentScreen = WelcomeScreen();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: PageStorage(
          child: currentScreen,
          bucket: bucket,
        ),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add),
          onPressed: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => const Dashboard()));
          },
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        bottomNavigationBar: BottomAppBar(
            shape: CircularNotchedRectangle(),
            notchMargin: 10,
            child: Container(
                height: 60,
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          MaterialButton(
                            minWidth: 100,
                            onPressed: () {
                              setState(() {
                                currentScreen = SignUpScreen();
                                currentTab = 0;
                              });
                            },
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Icon(
                                  Icons.arrow_forward,
                                  color: currentTab == 0
                                      ? Colors.blue
                                      : Colors.grey,
                                ),
                                Text(
                                  'SingUp',
                                  style: TextStyle(
                                      color: currentTab == 0
                                          ? Colors.blue
                                          : Colors.grey),
                                )
                              ],
                            ),
                          ),
                          MaterialButton(
                            minWidth: 40,
                            onPressed: () {
                              setState(() {
                                currentScreen = AddInput();
                                currentTab = 1;
                              });
                            },
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Icon(
                                  Icons.input,
                                  color: currentTab == 0
                                      ? Colors.blue
                                      : Colors.grey,
                                ),
                                Text(
                                  'Input',
                                  style: TextStyle(
                                      color: currentTab == 0
                                          ? Colors.blue
                                          : Colors.grey),
                                ),
                              ],
                            ),
                          ),
                          MaterialButton(
                            minWidth: 40,
                            onPressed: () {
                              setState(() {
                                currentScreen = Feature1();
                                currentTab = 1;
                              });
                            },
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Icon(
                                  Icons.anchor,
                                  color: currentTab == 0
                                      ? Colors.blue
                                      : Colors.grey,
                                ),
                                Text(
                                  'Feature 1',
                                  style: TextStyle(
                                      color: currentTab == 0
                                          ? Colors.blue
                                          : Colors.grey),
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                      //right navbar
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          MaterialButton(
                            minWidth: 40,
                            onPressed: () {
                              setState(() {
                                currentScreen = Feature2();
                                currentTab = 1;
                              });
                            },
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Icon(
                                  Icons.anchor,
                                  color: currentTab == 0
                                      ? Colors.blue
                                      : Colors.grey,
                                ),
                                Text(
                                  'Feature 2',
                                  style: TextStyle(
                                      color: currentTab == 0
                                          ? Colors.blue
                                          : Colors.grey),
                                ),
                              ],
                            ),
                          ),
                          MaterialButton(
                            minWidth: 100,
                            onPressed: () {
                              setState(() {
                                currentScreen = EditProfile();
                                currentTab = 0;
                              });
                            },
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Icon(
                                  Icons.person,
                                  color: currentTab == 0
                                      ? Colors.blue
                                      : Colors.grey,
                                ),
                                Text(
                                  'Profile',
                                  style: TextStyle(
                                      color: currentTab == 0
                                          ? Colors.blue
                                          : Colors.grey),
                                )
                              ],
                            ),
                          )
                        ],
                      )
                    ]))));
  }
}
