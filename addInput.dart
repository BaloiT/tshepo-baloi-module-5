import 'dart:developer';
import 'dart:html';

import 'package:app_system/AddInput.dart';
import 'package:app_system/inputlist.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class AddInput extends StatefulWidget {
  const AddInput({Key? key}) : super(key: key);

  @override
  State<AddInput> createState() => _AddInputState();
}

class _AddInputState extends State<AddInput> {
  @override
  Widget build(BuildContext context) {
    TextEditingController subjectController = TextEditingController();
    TextEditingController locationController = TextEditingController();

    Future _AddInput() {
      final subject = subjectController.text;
      final location = locationController.text;

      final ref = FirebaseFirestore.instance.collection("Details").doc();

      return ref
          .set(
              {"Subject_Name": subject, "location": location, "doc_id": ref.id})
          .then((value) =>
              {subjectController.text = "", locationController.text = ""})
          .catchError((onError) => log(onError));
    }

    return Column(
      children: [
        Column(
          children: [
            Padding(
                padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
                child: TextField(
                    controller: subjectController,
                    decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular((20)),
                        ),
                        hintText: "Enter Subject Name"))),
            Padding(
                padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
                child: TextField(
                    controller: locationController,
                    decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular((20)),
                        ),
                        hintText: "Enter Location"))),
            ElevatedButton(
                onPressed: () {
                  _AddInput();
                },
                child: Text("Add Session"))
          ],
        ),
        const InputList()
      ],
    );
  }
}
